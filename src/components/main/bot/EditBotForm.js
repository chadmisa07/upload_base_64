import React, { Component } from 'react'
import { Table, Button } from 'react-bootstrap'

class EditBot extends Component{

  render(){
    return (
      <div>
        <Table bordered hover className="form-padding" >
          <tbody>
            <tr>
              <th>Bot Name</th>
              <td>{ this.props.bot.name }</td>
              <td></td>
            </tr>
            <tr>
              <th>Bot ID</th>
              <td>{ this.props.bot.id }</td>
              <td><Button bsStyle="primary" bsSize="xsmall" block>Copy</Button></td>
            </tr>
          </tbody>
        </Table>
        <br/>
        <h4>Great!  Now define your Bot's job and associated flow.</h4>
        <br/>
        <h4>Job functions / Persona</h4>
        <Table bordered hover className="form-padding">
          <tbody>
            <tr>
              <th></th>
              <th className="text-center">Select Job or Persona</th>
              <th className="text-center">Select Flow</th>
              <th></th>
            </tr>
            <tr>
              <td>Job 1</td>
              <td className="text-center">Order</td>
              <td className="text-center">Order-001</td>
              <td><Button bsStyle="primary" bsSize="xsmall" block 
                    onClick={this.props.editOrderJob.bind(this)}>
                      Edit Job
                  </Button>
              </td>
            </tr>
            <tr>
              <td>Job 2</td>
              <td className="text-center">Survey</td>
              <td className="text-center">Survey-001</td>
              <td><Button bsStyle="primary" bsSize="xsmall" block>Edit Job</Button></td>
            </tr>
            <tr>
              <td>Default</td>
              <td className="text-center">FAQs</td>
              <td className="text-center">FAQ-001</td>
              <td><Button bsStyle="primary" bsSize="xsmall" block>Edit Job</Button></td>
            </tr>
          </tbody>
        </Table>
      </div>
    )
  }
}

export default EditBot