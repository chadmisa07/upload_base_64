import React from 'react'
import Header from '../../commons/header/Header'
import CreateBotSidebar from '../../commons/sidebar/CreateBotSidebar'
import EditBotForm from './EditBotForm'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'
import { browserHistory } from 'react-router'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user
  }
}

class EditBotContainer extends React.Component{

  editOrderJob() {
    browserHistory.push('/bot/order_menu')
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Header titleLeft="Create Your Bot" titleRight="Logout your account"/>
        </Row>
        <Row>
          <CreateBotSidebar selected="createBot" />
          <div className="page-content">
            <Col lg={6}>
              <EditBotForm bot={this.props.bot.bot}
                editOrderJob={this.editOrderJob} />
            </Col>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(EditBotContainer)