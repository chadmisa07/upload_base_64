import React from 'react'
import Header from '../../commons/header/Header'
import CreateBotSidebar from '../../commons/sidebar/CreateBotSidebar'
import CreateBotForm from './CreateBotForm'
import { connect } from 'react-redux'
import { Grid, Row, Col } from 'react-bootstrap'
import { createBot } from '../../../requests/bot'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    bot: state.bot,
    user: state.user,
    creating: false
  }
}

class CreateBotContainer extends React.Component{
  constructor(props) {
    super(props)
    this.state = {
      "platform":3,
      "name": "",
      "description": "",
      "created_by": props.user.id,
      "updated_by": props.user.id,
    }
  }

  createBot(){
    this.props.dispatch(createBot(this.state))
  }

  handlePlatformChange = (event) => {
    this.setState({ platform: event.target.value })
  }

  handleNameChange = (event) => {
    this.setState({ name: event.target.value })
  }

  handleDescriptionChange = (event) => {
    this.setState({ description: event.target.value })
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Header titleLeft="Create Your Bot" titleRight="Logout your account "/>
        </Row>
        <Row>
          <CreateBotSidebar selected="createBot" />
          <div className="page-content">
            <Col lg={6}>
              <CreateBotForm 
                platform={this.state.platform}
                name={this.state.name}
                description={this.state.description}
                handlePlatformChange={this.handlePlatformChange}
                handleNameChange={this.handleNameChange}
                handleDescriptionChange={this.handleDescriptionChange}
                createBot={this.createBot.bind(this)}
                creating={this.state.creating} />
            </Col>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(CreateBotContainer)