import React from 'react'
import Header from '../../../commons/header/Header'
import BotFlowEditorSideBar from '../../../commons/sidebar/BotFlowEditorSideBar'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { Grid, Row, Col, Table, ButtonGroup, Button, FormControl, ControlLabel, FormGroup, Form } from 'react-bootstrap'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  }
}

class OrderCountContainer extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      showModal: false
    }
  }

  close() {
    this.setState({ showModal: false });
  }

  edit = (event) => {
    this.setState({ showModal: true });
  }

  open() {
    this.setState({ showModal: true });
  }

  gotoNextPage() {
    browserHistory.push('/bot/order_cart')
  }

  render() {
    return (
      <Grid fluid>
        <Row>
          <Header titleLeft="Create Your Bot" titleRight="Logout your account "/>
        </Row>
        <Row>
          <BotFlowEditorSideBar selected="orderCount" />
          <div className="page-content">
            <Col lg={12}>
              <Table bordered className="form-padding half-width">
                <tbody>
                  <tr>
                    <td><b>Job Name</b></td>
                    <td>Order 001</td>
                    <td colSpan="2" className="save-next-btn">
                      <ButtonGroup justified>
                        <Button href="#" bsSize="xsmall" className="blue-text">Save</Button>
                        <Button href="#" bsSize="xsmall" className="blue-text" onClick={this.gotoNextPage.bind(this)}>Next</Button>
                      </ButtonGroup> 
                    </td>
                  </tr>
                  <tr>
                    <th>Associated Bot</th>
                    <td colSpan="3">Bot-ID Here</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col lg={6} className="form-padding">
              <Row>
                <Col lg={12}>
                  <h4 className="blue-text-color">Define Quantity interface</h4>
                </Col>
              </Row>
              <Row className="form-padding">
                <Col lg={12} className="form-padding">
                  <h4>2. Edit Text for asking how many orders when adding to cart</h4>
                  <Col md={12} className="form-padding">
                    <Form horizontal>
                      <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={3}>
                          Response Text
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" placeholder="Great! How many would you like?" />
                        </Col>
                      </FormGroup>

                      <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={3}>
                          Menu Selection Text
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" placeholder="Please choose or enter number" />
                        </Col>
                      </FormGroup>
                    </Form>
                  </Col>
                  <Col lg={12}>
                    <Form inline className="btn-padding">
                        <Col componentClass={ControlLabel} sm={3}>
                          <Button className="blue-text" bsSize="small">
                           + Add Number Option
                          </Button>
                        </Col>
                        <Col sm={9}>
                          <FormControl type="number" value={1} /> {' '}
                          <Button bsStyle="danger" bsSize="small">
                           <span className="glyphicon glyphicon-remove"></span>
                          </Button>
                        </Col>
                    </Form>
                    <Form inline className="btn-padding">
                        <Col componentClass={ControlLabel} sm={3}>
                          <Button className="blue-text" bsSize="small">
                           + Add Number Option
                          </Button>
                        </Col>
                        <Col sm={9}>
                          <FormControl type="number" value={2} /> {' '}
                          <Button bsStyle="danger" bsSize="small">
                           <span className="glyphicon glyphicon-remove"></span>
                          </Button>
                        </Col>
                    </Form>
                    <Form inline className="btn-padding">
                        <Col componentClass={ControlLabel} sm={3}>
                          <Button className="blue-text" bsSize="small">
                           + Add Number Option
                          </Button>
                        </Col>
                        <Col sm={9}>
                          <FormControl type="number" value={3} /> {' '}
                          <Button bsStyle="danger" bsSize="small">
                           <span className="glyphicon glyphicon-remove"></span>
                          </Button>
                        </Col>
                    </Form>
                    <Form inline className="btn-padding">
                        <Col componentClass={ControlLabel} sm={3}>
                          <Button className="blue-text" bsSize="small">
                           + Add Number Option
                          </Button>
                        </Col>
                        <Col sm={9}>
                          <FormControl type="number" value={4} /> {' '}
                          <Button bsStyle="danger" bsSize="small">
                           <span className="glyphicon glyphicon-remove"></span>
                          </Button>
                        </Col>
                    </Form>
                    <Form inline className="btn-padding">
                        <Col sm={3}>
                        </Col>
                        <Col sm={9}>
                          <FormControl type="number" value={5} /> {' '}
                          <Button bsStyle="danger" bsSize="small">
                           <span className="glyphicon glyphicon-remove"></span>
                          </Button>
                        </Col>
                    </Form>
                  </Col>
                </Col>
              </Row>
            </Col>
            <Col lg={6} className="form-padding">
              <div className="form-padding chatbox-border">
                <Row>
                  <p className="sender-message-bubble text-center pull-right">
                    Add to Cart
                  </p>
                </Row>
                <Row>
                  <p className="recipient-message-bubble text-center pull-left">
                    How Many?
                  </p>
                </Row>
                <Row>
                  <p className="recipient-message-bubble text-center pull-left">
                    Please choose how many:
                  </p>
                </Row>
                <Row className="text-center">
                  <Col lg={12}>
                      <p className="circle">
                        1
                      </p>
                      <p className="circle">
                        2
                      </p>
                      <p className="circle">
                        3
                      </p>
                      <p className="circle">
                        4
                      </p>
                      <p className="circle">
                        5
                      </p>
                  </Col>
                </Row>
              </div>
            </Col>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(OrderCountContainer)
