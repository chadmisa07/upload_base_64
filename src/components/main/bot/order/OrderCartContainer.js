import React from 'react'
import Header from '../../../commons/header/Header'
import BotFlowEditorSideBar from '../../../commons/sidebar/BotFlowEditorSideBar'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { Grid, Row, Col, Table, ButtonGroup, Button, FormControl, ControlLabel, FormGroup, Form } from 'react-bootstrap'
import { saveCart } from '../../../../requests/cart'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.user,
  }
}

class OrderCartContainer extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      responseText:"Item added.",
      menuSelectionText:"What do you want to do next?",
      orderMoreBtn: "Order More",
      viewCartBtn: "View Cart",
      checkoutBtn: "Checkout"
    }
  }

  handleResponseTextChange = (event) => {
    this.setState({ responseText: event.target.value })
  }

  handleMenuSelectionTextChange = (event) => {
    this.setState({ menuSelectionText: event.target.value })
  }

  handleOrderMoreBtnChange = (event) => {
    this.setState({ orderMoreBtn: event.target.value })
  }

  handleViewCartBtnChange = (event) => {
    this.setState({ viewCartBtn: event.target.value })
  }

  handleCheckoutBtnChange = (event) => {
    this.setState({ checkoutBtn: event.target.value })
  }

  goToNextPage() {
    browserHistory.push('/bot/order_delivery')
  }

  saveData(){
    let textResponseObj = {
                        "text_type":23,
                        "message": this.state.responseText,
                        "created_by": this.props.user.id,
                        "updated_by": this.props.user.id
                     }

    let  buttonObj= {
                            "text": this.state.menuSelectionText,
                            "button_type": 25,
                            "created_by": this.props.user.id,
                            "updated_by": this.props.user.id,
                            "buttons": [
                                          {
                                            "title": this.state.orderMoreBtn,
                                            "payload": this.state.orderMoreBtn,
                                            "button_type": 6,
                                            "created_by": this.props.user.id,
                                            "updated_by": this.props.user.id
                                          },
                                          {
                                            "title": this.state.viewCartBtn,
                                            "payload": this.state.viewCartBtn,
                                            "button_type": 6,
                                            "created_by": this.props.user.id,
                                            "updated_by": this.props.user.id
                                          },
                                          {
                                            "title": this.state.checkoutBtn,
                                            "payload": this.state.checkoutBtn,
                                            "button_type": 6,
                                            "created_by": this.props.user.id,
                                            "updated_by": this.props.user.id
                                          }
                                       ]
                    }

      this.props.dispatch(saveCart(textResponseObj, buttonObj))

      console.log(JSON.stringify(textResponseObj))
      console.log("======================")
      console.log(JSON.stringify(buttonObj))

  }

  render() {
    let bot_id = localStorage.getItem('bot_id')
    return (
      <Grid fluid>
        <Row>
          <Header titleLeft="Create Your Bot" titleRight="Logout your account "/>
        </Row>
        <Row>
          <BotFlowEditorSideBar selected="editCart" />
          <div className="page-content">
            <Col lg={12}>
              <Table bordered className="form-padding half-width">
                <tbody>
                  <tr>
                    <td><b>Job Name</b></td>
                    <td>Order 001</td>
                    <td colSpan="2" className="save-next-btn">
                      <ButtonGroup justified>
                        <Button href="#" bsSize="xsmall" className="blue-text" onClick={this.saveData.bind(this)}>Save</Button>
                        <Button href="#" bsSize="xsmall" className="blue-text" onClick={this.goToNextPage.bind(this)}>Next</Button>
                      </ButtonGroup> 
                    </td>
                  </tr>
                  <tr>
                    <th>Associated Bot</th>
                    <td colSpan="3">{bot_id}</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col lg={6} className="form-padding">
              <Row>
                <Col lg={12}>
                  <h4 className="blue-text-color">Define Cart Main Menu</h4>
                </Col>
              </Row>
              <Row className="form-padding">
                <Col lg={12} className="form-padding">
                  <h4>3. Cart</h4>
                  <Col md={12} className="form-padding">
                    <Form horizontal>
                      <FormGroup>
                        <Col componentClass={ControlLabel} sm={3}>
                          Response Text
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" placeholder="Item added." 
                            value={this.state.responseText}
                            onChange={this.handleResponseTextChange.bind(this)} />
                        </Col>
                      </FormGroup>

                      <FormGroup>
                        <Col componentClass={ControlLabel} sm={3}>
                          Menu Selection Text
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text"
                            value={this.state.menuSelectionText}
                            onChange={this.handleMenuSelectionTextChange.bind(this)}/>
                        </Col>
                      </FormGroup>

                      <FormGroup>
                        <Col componentClass={ControlLabel} sm={3}>
                          Order More
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" 
                            value={this.state.orderMoreBtn}
                            onChange={this.handleOrderMoreBtnChange.bind(this)}/>
                        </Col>
                      </FormGroup>

                      <FormGroup>
                        <Col componentClass={ControlLabel} sm={3}>
                          View Cart
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" 
                            value={this.state.viewCartBtn}
                            onChange={this.handleViewCartBtnChange.bind(this)}/>
                        </Col>
                      </FormGroup>

                      <FormGroup>
                        <Col componentClass={ControlLabel} sm={3}>
                          Checkout
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" 
                            value={this.state.checkoutBtn}
                            onChange={this.handleCheckoutBtnChange.bind(this)}/>
                        </Col>
                      </FormGroup>

                    </Form>
                  </Col>
                </Col>
              </Row>
            </Col>
            <Col lg={6} className="form-padding">
              <div>
                <div className="form-padding chatbox-border">
                  <Row>
                    <div className="recipient-message-bubble text-center pull-left">
                      { this.state.responseText }
                    </div>
                  </Row>
                  <Row className="padding-top-15">
                    <div className="text-center pull-left">
                      <div className="btn-response-text text-center pull-left">
                        { this.state.menuSelectionText }
                      </div>
                      <div className="btn-response-button text-center pull-left">
                        { this.state.orderMoreBtn }
                      </div>
                      <div className="btn-response-button text-center pull-left">
                        { this.state.viewCartBtn }
                      </div>
                      <div className="btn-response-button text-center pull-left">
                        { this.state.checkoutBtn }
                      </div>
                    </div>
                  </Row>
                </div>
              </div>
            </Col>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(OrderCartContainer)
