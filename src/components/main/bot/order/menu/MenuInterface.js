import React from 'react'
import { Thumbnail, Button, ButtonGroup } from 'react-bootstrap'

class MenuInterface extends React.Component{
  render() {
    return (
      <div className="card-padding">
        <Thumbnail src={this.props.product.image_url}
          className="fb-button">
          <h3 className="fb-title">{this.props.product.name}</h3>
          <p className="fb-description">{this.props.product.description}</p>
          <p className="product-url">{this.props.product.item_url}</p>
            <ButtonGroup vertical block>
              <Button className="fb-button">{this.props.add_button}</Button>
              <Button className="fb-button">{this.props.view_description}</Button>
            </ButtonGroup>
        </Thumbnail>
      </div>
    )
  }
}

export default MenuInterface