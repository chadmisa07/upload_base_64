import React from 'react'
import Header from '../../../commons/header/Header'
import BotFlowEditorSideBar from '../../../commons/sidebar/BotFlowEditorSideBar'
import { connect } from 'react-redux'
import { browserHistory } from 'react-router'
import { Grid, Row, Col, Table, ButtonGroup, Button, FormControl, ControlLabel, FormGroup, Form } from 'react-bootstrap'
import { checkout } from '../../../../requests/checkout'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
    user: state.user,
  }
}

class OrderCheckoutContainer extends React.Component{

  constructor(props) {
    super(props)
    this.state = {
      checkoutText:"Proceed with checkout?",
      askReceiptText:"Do you want a receipt?",
      askEmailText: "Please provide email"
    }
  }

  gotoNextPage() {
    alert('Bot has now been saved!')
    browserHistory.push('/main')
  }

  handleCheckoutTextChange = (event) => {
    this.setState({ checkoutText: event.target.value })
  }

  handleAskReceiptTextChange = (event) => {
    this.setState({ askReceiptText: event.target.value })
  }

  handleAskEmailTextChange = (event) => {
    this.setState({ askEmailText: event.target.value })
  }

  saveData(){
    let data = [
                  {
                    quick_replies: [
                      {"title":"Yes","payload":"YES","reply_type":14,"created_by":this.props.user.id,"updated_by":this.props.user.id},
                      {"title":"No","payload":"NO","reply_type":14,"created_by":this.props.user.id,"updated_by":this.props.user.id}
                    ],"text": this.state.checkoutText, qr_type: 17, "updated_by":this.props.user.id
                  },
                  {
                    quick_replies: [
                    {"title":"Yes","payload":"YES","reply_type":14,"created_by":this.props.user.id,"updated_by":this.props.user.id},
                    {"title":"No","payload":"NO","reply_type":14,"created_by":this.props.user.id,"updated_by":this.props.user.id}
                    ],"text": this.state.askReceiptText, qr_type: 17, "updated_by":this.props.user.id
                  },
                  {
                    quick_replies: [
                    {"title":"Yes","payload":"YES","reply_type":14,"created_by":this.props.user.id,"updated_by":this.props.user.id},
                    {"title":"No","payload":"NO","reply_type":14,"created_by":this.props.user.id,"updated_by":this.props.user.id}
                    ],"text": this.state.askEmailText, qr_type: 17, "updated_by":this.props.user.id
                  }
               ]

      this.props.dispatch(checkout(data))
      console.log(JSON.stringify(data))
  } 

  render() {
    let bot_id = localStorage.getItem('bot_id')
    return (
      <Grid fluid>
        <Row>
          <Header titleLeft="Create Your Bot" titleRight="Logout your account "/>
        </Row>
        <Row>
          <BotFlowEditorSideBar selected="editCheckout" />
          <div className="page-content">
            <Col lg={12}>
              <Table bordered className="form-padding half-width">
                <tbody>
                  <tr>
                    <td><b>Job Name</b></td>
                    <td>Order 001</td>
                    <td colSpan="2" className="save-next-btn">
                      <ButtonGroup justified>
                        <Button href="#" bsSize="xsmall" className="blue-text" onClick={this.saveData.bind(this)}>Save</Button>
                        <Button href="#" bsSize="xsmall" className="blue-text" onClick={this.gotoNextPage.bind(this)}>Next</Button>
                      </ButtonGroup> 
                    </td>
                  </tr>
                  <tr>
                    <th>Associated Bot</th>
                    <td colSpan="3">{bot_id}</td>
                  </tr>
                </tbody>
              </Table>
            </Col>
            <Col lg={6} className="form-padding">
              <Row>
                <Col lg={12}>
                  <h4 className="blue-text-color">Define Checkout Options</h4>
                </Col>
              </Row>
              <Row className="form-padding">
                <Col lg={12} className="form-padding">
                  <h4>5. Edit checkout questions</h4>
                  <Col md={12} className="form-padding">
                    <Form horizontal>
                      <FormGroup controlId="formHorizontalEmail">
                        <Col componentClass={ControlLabel} sm={3}>
                          Checkout
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" placeholder="Proceed to checkout?" 
                            value={this.state.checkoutText}
                            onChange={this.handleCheckoutTextChange.bind(this)} />
                        </Col>
                      </FormGroup>

                      <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={3}>
                          Email Receipt
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" placeholder="Do you want a receipt?"
                            value={this.state.askReceiptText}
                            onChange={this.handleAskReceiptTextChange.bind(this)} />
                        </Col>
                      </FormGroup>

                      <FormGroup controlId="formHorizontalPassword">
                        <Col componentClass={ControlLabel} sm={3}>
                          Ask for Email
                        </Col>
                        <Col sm={9}>
                          <FormControl type="text" placeholder="Please provide email"
                            value={this.state.askEmailText}
                            onChange={this.handleAskEmailTextChange.bind(this)} />
                        </Col>
                      </FormGroup>
                    </Form>
                  </Col>
                </Col>
              </Row>
            </Col>
            <Col lg={6} className="form-padding">
              <div className="form-padding chatbox-border">
                <Row>
                  <p className="sender-message-bubble text-center pull-right">
                    Checkout
                  </p>
                </Row>
                <Row>
                  <p className="recipient-message-bubble text-center pull-left">
                    { this.state.checkoutText }
                  </p>
                </Row>
                <Row className="text-center">
                  <Col lg={12}>
                    <p className="quick-reply-btn">
                      Yes
                    </p>
                    <p className="quick-reply-btn">
                      No
                    </p>
                  </Col>
                </Row>
                <Row>
                  <p className="recipient-message-bubble text-center pull-left">
                    { this.state.askReceiptText }
                  </p>
                </Row>
                <Row className="text-center">
                  <Col lg={12}>
                    <p className="quick-reply-btn">
                      Yes
                    </p>
                    <p className="quick-reply-btn">
                      No
                    </p>
                  </Col>
                </Row>
                <Row>
                  <p className="recipient-message-bubble text-center pull-left">
                    { this.state.askEmailText }
                  </p>
                </Row>
                <Row className="text-center">
                  <Col lg={12}>
                    <p className="quick-reply-btn">
                      Yes
                    </p>
                    <p className="quick-reply-btn">
                      No
                    </p>
                  </Col>
                </Row>
              </div>
            </Col>
          </div>
        </Row>
      </Grid>
    )
  }
}

export default connect(mapStateToProps)(OrderCheckoutContainer)
