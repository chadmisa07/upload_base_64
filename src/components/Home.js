import React from 'react'
import '../includes/css/home.css'
import { Col, Grid, Row, Table } from 'react-bootstrap'
import { Link } from 'react-router'

class Home extends React.Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col md={12}>
            <Link to="logout" className="pull-right row-padding">Logout</Link>
            <Table bordered condensed>
              <tbody>
                <tr>
                  <td className="column-title">Facebook Pages</td>
                  <td><Link to="/bot/create_bot">Create a Page</Link></td>
                </tr>
                <tr>
                  <td className="column-title">Bot Creation</td>
                  <td><Link to="bot/create_bot">Create Bot</Link></td>
                  <td><Link to="bot/edit_bot">Edit Bot</Link></td>
                  <td><Link to="bot/edit_bot_jobs">Edit Bot Jobs</Link></td>
                </tr>
                <tr>
                  <td className="column-title">Bot Administration</td>
                  <td><Link to="admin/manage_bot">Manage Bot Settings</Link></td>
                  <td><Link to="admin/administer_bots">Administer Created Bots</Link></td>
                </tr>
                <tr>
                  <td className="column-title">Business Process Management</td>
                  <td><Link to="business/order">Order Management</Link></td>
                  <td><Link to="business/faq">FAQ Management</Link></td>
                  <td><Link to="business/survey">Survey Management</Link></td>
                </tr>
              </tbody>
            </Table>
          </Col>
        </Row>
      </Grid>
    )
  }
}

export default Home