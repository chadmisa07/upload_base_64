import React from 'react'
import { connect } from 'react-redux'

import { login } from '../actions/loginActions'

import { Grid, Row, Col } from 'react-bootstrap'
import { Form, FormGroup, FormControl } from 'react-bootstrap'
import { ControlLabel, Button, Panel, Alert } from 'react-bootstrap'

const mapStateToProps = (state) => {
  return {
    auth: state.auth,
  }
}

class LoginForm extends React.Component {

  login() {
    let username = this.state.usernameInput
    let password = this.state.passwordInput
    this.props.dispatch(login(username, password))
  }

  handleKeyPress = (event) => {
    if(event.key === 'Enter'){
      this.login()
    }
  }

  handleUsernameChange = (event) => {
      this.setState({ usernameInput: event.target.value })
  }

  handlePasswordChange = (event) => {
      this.setState({ passwordInput: event.target.value })
  }

  render() {
		return (
        <Grid>
          <Row className="row-padding">
            <Col md={6} mdOffset={3} >
              <Panel header="Chatbot System" bsStyle="primary">
                {
                  this.props.auth.error != null? 
                  <Alert bsStyle="danger" className="text-center">{this.props.auth.error }</Alert>: null
                }
                <Form horizontal>
                  <FormGroup controlId="formHorizontalEmail">
                    <Col componentClass={ControlLabel} sm={3}>
                      Username:
                    </Col>
                    <Col sm={9}>
                      <FormControl type="text" placeholder="Username" onChange={this.handleUsernameChange} />
                    </Col>
                  </FormGroup>
                  <FormGroup controlId="formHorizontalPassword">
                    <Col componentClass={ControlLabel} sm={3}>
                      Password:
                    </Col>
                    <Col sm={9}>
                      <FormControl type="password" placeholder="Password" onChange={this.handlePasswordChange} 
                        onKeyPress={this.handleKeyPress.bind(this)}/>
                    </Col>
                  </FormGroup>

                  <FormGroup>
                    <Col smOffset={3} sm={9}>
                      <Button type="button" bsStyle="info" bsSize="small" 
                        disabled={this.props.auth.fetching} onClick={this.login.bind(this)} >
                          {this.props.auth.fetching ? 'Signing in...' : 'Sign in'}
                      </Button>
                    </Col>
                  </FormGroup>
                </Form>
              </Panel>
            </Col>
          </Row>
        </Grid>
			)
  }
}

export default connect(mapStateToProps)(LoginForm)
