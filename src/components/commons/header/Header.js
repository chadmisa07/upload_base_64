import React, { Component } from 'react'
import { Grid, Row, Col } from 'react-bootstrap'

class Header extends Component{
  render(){
    return (
      <div className="header">
        <Grid fluid>
          <Row>
            <Col lg={6}>
              <a href="/home"><p className="header-text-left pull-left">Chatbot System</p></a>
            </Col>
            <Col lg={6}>
                <a className="pull-right header-text-right" href="/logout" >Logout</a>
            </Col>
          </Row>
        </Grid>
      </div>
    )
  }
}

export default Header