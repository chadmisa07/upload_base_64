export default function reducer(state={
	quickReply: [],
	saving: false,
	fetching: false,
	success: false,
	failed: false,
	nextButtonDisabled: true,
	error: null,
}, action){

	switch(action.type)
	{case "CHECKOUT_REQUEST": {
			return {...state, saving: true}
		}
		case "CHECKOUT_REQUEST_FULFILLED":{
			// return {...state, saving: false, success: true, image_url: action.image_url, title: action.title, description: action.description, button_text: action.button_text}
			return {...state, saving: false, nextButtonDisabled: false, success: true, quickReply:action.payload}
		}
		case "CHECKOUT_REQUEST_REJECTED":{
			return {...state, saving: false, failed: true ,error: action.payload}
		}

		default: {
			return state;
		}
	}
	
}