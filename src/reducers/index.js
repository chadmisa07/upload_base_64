import { combineReducers } from 'redux'

import auth from './auth'
import bot from './bot'
import user from './user'
import textResponse from './textResponse'
import menuInterface from './menuInterface'
import delivery from './delivery'
import cart from './cart'
import checkout from './checkout'
import {reducer as toastrReducer} from 'react-redux-toastr'

export default combineReducers({
  auth,
  bot,
  user,
  toastr: toastrReducer,
  textResponse,
  menuInterface,
  delivery,
  cart,
  checkout
})
