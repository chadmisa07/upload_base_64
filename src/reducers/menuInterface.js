export default function reducer(state, action) {
  switch(action.type) {
    case "MENU_INTERFACE_CREATED": {
      return {
        ...state,
        arr: [...state.arr, action.payload]
      }
    }
    default: {
      return {...state}
    }
  }
}