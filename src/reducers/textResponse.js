export default function reducer(state, action) {
  switch(action.type) {
    case "TEXT_RESPONSE_CREATED": {
      return {
        ...state,
        arr: [...state.arr, action.payload]
      }
    }
    default: {
        return {...state}
    }
  }
}