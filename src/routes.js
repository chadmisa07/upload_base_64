import React from 'react'
import { IndexRoute, Route, Router, browserHistory } from 'react-router'
import { Provider } from 'react-redux'

import App from './components/App'
import Home from './components/Home'
import LoginForm from './components/LoginForm'
import Logout from './components/Logout'
import CreateBotContainer from './components/main/bot/CreateBotContainer'
import EditBotContainer from './components/main/bot/EditBotContainer'
import OrderMenuContainer from './components/main/bot/order/menu/OrderMenuContainer'
import OrderCountContainer from './components/main/bot/order/OrderCountContainer'
import OrderCartContainer from './components/main/bot/order/OrderCartContainer'
import OrderDeliveryContainer from './components/main/bot/order/OrderDeliveryContainer'
import OrderCheckoutContainer from './components/main/bot/order/OrderCheckoutContainer'

import store from './store/store'
import requireAuth from './utils/requireAuth'
import ReduxToastr from 'react-redux-toastr'


const Routes = () => {
    return (
      <Provider store={store}>
        <div>
          <Router history={browserHistory}>
            <Route path="/" component={LoginForm} />
            <Route path="/login" component={LoginForm} />
            <Route path="/home" component={requireAuth(Home)} />
            <Route path="/logout" component={Logout} />
            <Route path="/bot" component={requireAuth(App)} >
              <IndexRoute component={CreateBotContainer} />
              <Route path="/bot/create_bot" component={CreateBotContainer} />
              <Route path="/bot/edit_bot" component={EditBotContainer} />
              <Route path="/bot/order_menu" component={OrderMenuContainer} />
              <Route path="/bot/order_count" component={OrderCountContainer} />
              <Route path="/bot/order_cart" component={OrderCartContainer} />
              <Route path="/bot/order_delivery" component={OrderDeliveryContainer} />
              <Route path="/bot/checkout" component={OrderCheckoutContainer} />
            </Route>
            <Route path="*" component={requireAuth(Home)} />
          </Router>
          <ReduxToastr
            timeOut={5000}
            newestOnTop={true}
            preventDuplicates={false}
            position="top-right"
            transitionIn="fadeIn"
            transitionOut="fadeOut"
            progressBar/>
        </div>
      </Provider>
    )
}

export default Routes