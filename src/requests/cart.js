import axios from 'axios'
import { toastr } from 'react-redux-toastr'
import { browserHistory } from 'react-router'

export function buttonSave(obj){

	return function(dispatch)
	{
		dispatch({type: "CART_BUTTON_SAVE_REQUEST" })
		axios.post("https://cbot-api.herokuapp.com/api/button-response/", obj)
		.then(function (response) {
			dispatch({type: "CART_BUTTON_SAVE_REQUEST_FULFILLED", payload: response.data})
			toastr.success('Success', 'Cart flow successfully saved')
        	browserHistory.push('/bot/order_delivery')
		})
		.catch(function (error) {
			dispatch({type: "CART_BUTTON_SAVE_REQUEST_REJECTED", payload: error})
			toastr.error('Error', 'Failed to save Cart flow. Please try again.')
		});
	}
}


export function saveCart(textResposnseObj, buttonsObj){

	return function(dispatch)
	{
		dispatch({type: "TEXT_RESPONSE_SAVE_REQUEST" })
		axios.post("https://cbot-api.herokuapp.com/api/text-response/", textResposnseObj)
		.then(function (response) {
			dispatch({type: "TEXT_RESPONSE_SAVE_REQUEST_FULFILLED", payload: response.data})
			dispatch(buttonSave(buttonsObj))
		})
		.catch(function (error) {
			dispatch({type: "TEXT_RESPONSE_SAVE_REQUEST_REJECTED", payload: error})
			alert("Sorry, an error occured. Please try again.");
		});
	}
}

