import axios from 'axios'
import { browserHistory } from 'react-router'
import { toastr } from 'react-redux-toastr'

export function createBot(data) {
  return function(dispatch) {
    dispatch({type: "CREATE_BOT_REQUEST" })
    axios.post("https://cbot-api.herokuapp.com/api/bot/", data)
      .then(function (response) {
        dispatch({type: "CREATE_BOT_REQUEST_FULFILLED", payload: response.data})
        toastr.success('Success', 'Bot was created')
        browserHistory.push('/bot/edit_bot')
      })
      .catch(function (error) {
        toastr.error('Error', 'Please enter valid inputs')
        dispatch({type: "CREATE_BOT_REQUEST_REJECTED", payload: error})
      })
  }  
}