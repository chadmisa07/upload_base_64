import axios from 'axios'
import { browserHistory } from 'react-router'

export function getUser(username){
  return function(dispatch) {
    axios.get("https://cbot-api.herokuapp.com/api/user/?username=" + username)
      .then(function (response) {
        dispatch({type: "FETCHED_USER", payload: response.data.results[0]})
        browserHistory.push("/home")
      })
      .catch(function (error) {
        alert("nirequest?")
      })
  }  
}