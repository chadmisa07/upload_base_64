import axios from 'axios'
import { toastr } from 'react-redux-toastr'

export function createTextResponse(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/text-response/", data)
      .then(function (response) {
        dispatch({type: "TEXT_RESPONSE_CREATED", payload: response.data})
        toastr.success('Success', 'Welcome message was created')
      })
      .catch(function (error) {
        toastr.error('Error', 'Unable to create welcome message')
      })
  }  
}