import axios from 'axios'
import  { createMenuInterface } from './menuInterface'
import { toastr } from 'react-redux-toastr'

export function createProduct(data) {
  return function(dispatch) {
    axios.post("https://cbot-api.herokuapp.com/api/product-item/", data.product)
      .then(function (response) {
        toastr.success('Success', 'Product was created')
        data.product = response.data.id
        dispatch(createMenuInterface(data))
      })
      .catch(function (error) {
        toastr.error('Error', 'Error creating product')
      })
  }  
}